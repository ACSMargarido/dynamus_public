<?php

/**
 * HOTP Class
 * Based on the work of OAuth, and the sample implementation of HMAC OTP
 * http://tools.ietf.org/html/draft-mraihi-oath-hmac-otp-04#appendix-D
 *
 * Based on https://github.com/Jakobo/hotp-php
 *
 * @author Alberto Margarido
 * @copyright 2016
 * @license BSD
 * @version 1.2
 */

 
 
 /*
 History
 
 1.2 -  Testa -> class_exists
 1.1 -  Configuração para o número de caracteres de resposta
 */

/*  Final lenght of the output. Default 8 digits   */ 
define('DynamusTOTP_LENGHT_OUTPUT',8);   
 

if (! class_exists ( 'DynamusTOTP' )) { 
class DynamusTOTP {

	public static function dynamusByCounter($key, $rounded_time) {
    	
    	// Pack the counter into binary
    	$packed_time = pack("N", $rounded_time);
    	
    	// Make sure the packed time is 8 characters long
    	$padded_packed_time = str_pad($packed_time,DynamusTOTP_LENGHT_OUTPUT, chr(0), STR_PAD_LEFT);
    	
    	// Pack the secret seed into a binary string
    	$packed_secret_seed = pack("H*", $key);
    	
    	// Generate the hash using the SHA1 algorithm
    	$hash = hash_hmac('sha1', $padded_packed_time, $packed_secret_seed, true);
    	
    	return new DynamusTOTPResult($hash);
    	
    }
    
    public static function dynamusByTime($key, $window, $timestamp = false) {
    	
    	if (!$timestamp && $timestamp !== 0) {
    		$timestamp = DynamusTOTP::getTime();
    	}
    
    	$oldCounter = intval($timestamp / $window);
    	$counter = floor($timestamp / $window);
    	
    	return DynamusTOTP::dynamusByCounter($key, $counter);
    }    
    
    
    public static function dynamusByTimeWindow($key, $window, $min = -1, $max = 1, $timestamp = false) {
    	
    	if (!$timestamp && $timestamp !== 0) {
    		$timestamp = DynamusTOTP::getTime();
    	}
    	 
    	$rounded_time = floor($timestamp / $window); 
    	$window = range($min, $max);
    	

    	$out = array();
    	for ($i = 0; $i < count($window); $i++) {
    		$shift_counter = $window[$i];
     		$out[$shift_counter] = DynamusTOTP::dynamusByCounter($key, $rounded_time + $shift_counter);
    	}
    	
    	return $out;
    	
    }
    
    public static function getTime() {
        return time(); // PHP's time is always UTC
    }
} // class

class DynamusTOTPResult {
    protected $hash;
    protected $binary;
    protected $decimal;
    protected $hex;
    
    /**
     * Build an DynamusTOTP Result
     * @param string $value the value to construct with
     */
    public function __construct($value) {
        // store raw
        $this->hash = $value;
    }
    
    /**
     * Returns the string version of the DynamusTOTP
     * @return string
     */
    public function toString() {
        return $this->hash;
    }
    
    /**
     * Returns the hex version of the DynamusTOTP
     * @return string
     */
    public function toHex() {
        if( !$this->hex ) 
        {
            $this->hex = dechex($this->toDec());
        }
        return $this->hex;
    }
    
    /**
     * Returns the decimal version of the DynamusTOTP
     * @return int
     */
    public function toDec() {
        if( !$this->decimal ) 
        {
          
            $offset = ord($this->hash[19]) & 0xf;
            $this->decimal = (
              		((ord($this->hash[$offset+0]) & 0x7f) << 24 ) |
               		((ord($this->hash[$offset+1]) & 0xff) << 16 ) |
               		((ord($this->hash[$offset+2]) & 0xff) << 8  ) |
               		( ord($this->hash[$offset+3]) & 0xff)
            ) % pow(10, DynamusTOTP_LENGHT_OUTPUT);
            
            
        }
        return str_pad($this->decimal, DynamusTOTP_LENGHT_OUTPUT, "0", STR_PAD_LEFT);
    }
} // class
} // class_exists



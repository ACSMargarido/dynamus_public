<?php
/**
 * Sample class TOTP call
 *
 * @author Alberto Margarido
 * @copyright 2016
 * @license BSD
 * @version 1.0
 */



require_once 'DynamusTOTP.php';

define ( 'DYN_TOKEN_KEY',"3132333435363738393031323334353637383930"); // Must be a STRING !

$dynamus_token = DynamusTOTP::dynamusByTime(DYN_TOKEN_KEY, 120);
echo "</br>dynamus_token: " . $dynamus_token->toDec();


/*
 *  OLD sample 

$computed_token = DynamusTOTP::dynamusByTimeWindow(DYN_TOKEN_KEY, 120, -3, 3 );

echo ( "</br>" );

echo ( 'computed_token[-3]->toDec() : '. $computed_token[-3]->toDec() .  "</br>" );
echo ( 'computed_token[-2]->toDec() : '. $computed_token[-2]->toDec() . "</br>");
echo ( 'computed_token[-1]->toDec() : '. $computed_token[-1]->toDec() . "</br>");
echo ( 'computed_token[0]->toDec()  : '. $computed_token[0]->toDec() . "</br>" );
echo ( 'computed_token[1]->toDec()  : '. $computed_token[1]->toDec() . "</br>");
echo ( 'computed_token[2]->toDec()  : '. $computed_token[2]->toDec() . "</br>" );
echo ( 'computed_token[3]->toDec()  : '. $computed_token[3]->toDec() . "</br>");

echo ( "</br>" );
echo ( "</br>" );

*/


?>
<!-- Biblioteca utilizada: https://github.com/Caligatio/jsSHA -->
<script src="/diretorio/para/sha.js"></script>
    <script type="text/javascript">

        TOTP = function()
        {
            var dec2hex = function(s)
            {
                return (s < 15.5 ? "0" : "") + Math.round(s).toString(16);
            };

            var hex2dec = function(s)
            {
                return parseInt(s, 16);
            };

            var leftpad = function(str, len, pad)
            {
                if(len + 1 >= str.length)
                {
                    str = Array(len + 1 - str.length).join(pad) + str;
                }
                return str;
            };

            this.getOTP = function(key)
            {
                try
                {
                    var now = new Date();
                    var utc_timestamp = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate() , now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
                    var epoch = Math.round(utc_timestamp / 1000.0);
                    var time = leftpad(dec2hex(Math.floor(epoch / 120)), 16, '0');
                    var shaObj = new jsSHA("SHA-1", "HEX");
                    shaObj.setHMACKey(key, "HEX");
                    shaObj.update(time);
                    var hmac = shaObj.getHMAC("HEX");
                    var offset = hex2dec(hmac.substring(hmac.length - 1));
                    var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) + '';
                    otp = (otp).substr(otp.length - 8, 8);
                } catch (error) {
                    throw error;
                }
                return otp;
            };
        }

        function timer()
        {
            var epoch = Math.round(new Date().getTime() / 1000.0);
            var countDown = 30 - (epoch % 30);
            if (epoch % 30 == 0) updateOtp();
        }

        $( document ).ready(function()
        {
            setTimeout(updateOtp, 1000);
            setInterval(timer, 1000);
        });
        function updateOtp() {
            var totpObj = new TOTP();
            var otp = totpObj.getOTP("SUBSTITUA AQUI O SEU DYNAMUS TOKEN"); // SUBSTITUA AQUI O SEU DYNAMUS TOKEN
            $('#otp').val(otp);
        }
    </script>
